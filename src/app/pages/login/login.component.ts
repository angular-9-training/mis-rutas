import { Component, OnInit, OnDestroy } from '@angular/core';

import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;

  authSubscription: Subscription[] = [];

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.authSubscription.forEach(el => el.unsubscribe());
  }

  login() {
    this.authSubscription.push(
      this.authService.authenticate(this.username, this.password).subscribe(
        (loggedIn) => {
          if (loggedIn.token) {
            this.router.navigate(['/home']);
          } else {
            //this.username = "";
            //this.password = "";
            alert('Login incorrecto');
          }
        }
    ));
  }



}
