import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UsersService } from 'src/app/services/users.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.sass']
})
export class UserDetailsComponent implements OnInit {

  id: number;
  user: User;
  constructor(private usersService: UsersService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(
      params => {
        this.id = params.id;
        this.usersService.get_user(this.id)
          .subscribe(
            data => {
              this.user = data;
            }
            );
      }
    );  
    
  }

  backToList() {
    this.router.navigate(['/users']);
  }

}
