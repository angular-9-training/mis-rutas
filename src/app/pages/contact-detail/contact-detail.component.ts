import { Component, OnInit } from '@angular/core';

//Importar de @angular/router los elementos necesarios
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.sass']
})
export class ContactDetailComponent implements OnInit {

  idContacto: number;

  constructor(private route: ActivatedRoute, private router: Router ) { }

  ngOnInit(): void {
    // this.idContacto = this.route.snapshot.params.id;
    // console.log(this.idContacto);

    this.route.params.subscribe(
      params => {
        this.idContacto = params.id;
      });
        
    console.log(this.idContacto);

  }

  backToList() {
    this.router.navigate(['/contacts'])
  }

}
