import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ReqresUsersResponse } from 'src/app/models/reqres-users-response';
import { UsersService } from 'src/app/services/users.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.sass']
})
export class UsersComponent implements OnInit {

  currentPage: number = 1;
  totalPages: number;
  users: User[];
  usersResponse: ReqresUsersResponse;

  constructor(private router: Router, private usersService: UsersService) { }

  ngOnInit(): void {
    this.getUsersInPage(this.currentPage);
  }

  getUsersInPage(page: number) {
    this.currentPage = page;
    this.usersService.get_users(this.currentPage).subscribe(response => {
      this.usersResponse = response;
      this.users = response.data;
      this.totalPages = this.usersResponse.total_pages;
    });
  }

  nextPage() {
    if (this.currentPage < this.usersResponse.total_pages ) {
      this.currentPage += 1;
    }
    this.getUsersInPage(this.currentPage);
  }

  prevPage() {
    if ( this.currentPage > 1) {
      this.currentPage -= 1;
    }
    this.getUsersInPage(this.currentPage);
  }

  irAlDetalle(id: number) {
    this.router.navigate(['/users', id ]);
  }

}
