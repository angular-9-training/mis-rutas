import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

//Importamos HttpClient 
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { ReqresUsersResponse } from '../models/reqres-users-response';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  baseUri = 'https://reqres.in/api';

  constructor(private http: HttpClient) { }

  get_users(page: number): Observable<ReqresUsersResponse> {
    const params = new HttpParams()
      .set('page', page.toString());
    const req$ = this.http.get(this.baseUri + '/users', { params })
      .pipe(
        map(
          response => response as ReqresUsersResponse
        )
      );
    return req$;
  }

  get_user(id: number): Observable<User> {
    const req$ = this.http.get(this.baseUri + '/users/' + id).
      pipe(
        map(
          response => {
            let user = new User();
            user = response['data'];
            user.ad = response['ad'];
            return user;
          }
        )
      );
    return req$;
  }

}
