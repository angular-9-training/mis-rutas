import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';


//Importamos HttpClient 
import { HttpClient } from '@angular/common/http';

//
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  loggedIn = false;
  baseUri = 'https://reqres.in/api';

  constructor(private http: HttpClient) { }

  authenticate(username: string, password: string): Observable<any> {
    const body = { email: username, password };
    console.log(body);
    const req$ = this.http.post<any>(this.baseUri + '/login', body);
    req$.subscribe(
      (response) => {
        console.table(response);
        if (response.token !== null) {
          this.loggedIn = true;
          of(this.loggedIn);
        } else {
          this.loggedIn = false;
          of(this.loggedIn);
        }
      }
    );

    return req$;
  }

  get logged() {
    return this.loggedIn;
  }


}
