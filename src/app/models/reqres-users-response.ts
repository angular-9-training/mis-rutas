import { User } from './user';

export class ReqresUsersResponse {

  page: number;
  per_page: number;
  total: number;
  total_pages: number;
  data: User[];

}


