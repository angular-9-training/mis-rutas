export class User {
  id: number;
  email: string;
  first_name: number;
  last_name: string;
  avatar: string;

  ad?: Ad;

}

export class Ad {
  company: string;
  url: string;
  text: string;
}
